package javarogue.config;

import javarogue.utility.Resolution;

/**
 * 
 * (Config)uration is a collection of static values used for project-wise
 * "constants" initialized at the moment of building. Pre-builder values are set
 * to null.
 * 
 * Note that the use of static primitives is deprecated, because they cannot be
 * set to null prior to calling the builder.
 *
 */
public class ConfigGraphics {

	/**
	 * The game window resolution width in pixels.
	 */
	public static Integer resolutionWidth = null;
	/**
	 * The game window resolution height in pixels.
	 */
	public static Integer resolutionHeight = null;
	/**
	 * The tileset's single tile size (16 x 16, 32 x 32 etc...)
	 */
	public static Integer tileSize = null;
	/**
	 * The scale of rendered image.
	 */
	public static Double renderScale = null;
	/**
	 * The scale of the saved Tileset, impacts performance!
	 */
	public static Integer rasterScale = null;
	/**
	 * 
	 */
	public static String tilesetPath = null;

	/**
	 * Set config's values. Must be called prior to launching the game window.
	 * 
	 * @param resolution selected resolution
	 * @param tileSize   selected tile size
	 */
	public ConfigGraphics(Resolution resolution, int tileSize, double renderScale, Integer rasterScale, String tilesetPath) {
		ConfigGraphics.resolutionWidth = resolution.getWidth();
		ConfigGraphics.resolutionHeight = resolution.getHeight();
		ConfigGraphics.tileSize = tileSize;
		ConfigGraphics.renderScale = renderScale;
		ConfigGraphics.rasterScale = rasterScale;
		ConfigGraphics.tilesetPath = "res/tileset_" + tilesetPath + ".png";
	}

}
