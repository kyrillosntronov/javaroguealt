package javarogue.utility;

/**
 * 
 * Represents various resolutions of a window. Has a method to provide window
 * width and height. Overrides toString to represent resolution in a
 * conventional way.
 *
 */
public enum Resolution {

	HD {

		@Override
		public int getWidth() {
			return 1366;
		}

		@Override
		public int getHeight() {
			return 768;
		}

		@Override
		public String toString() {
			return "1366 x 768";
		}

	},
	FHD {

		@Override
		public int getWidth() {
			return 1920;
		}

		@Override
		public int getHeight() {
			return 1080;
		}

		@Override
		public String toString() {
			return "1920 x 1080";
		}

	},
	SVGA {
		@Override
		public int getWidth() {
			return 800;
		}

		@Override
		public int getHeight() {
			return 600;
		}

		@Override
		public String toString() {
			return "800 x 600";
		}
	};

	/**
	 * 
	 * @return Resolution's width
	 */
	public abstract int getWidth();

	/**
	 * 
	 * @return Resolutuons's Height
	 */
	public abstract int getHeight();

}
