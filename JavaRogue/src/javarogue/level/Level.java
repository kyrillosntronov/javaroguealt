package javarogue.level;

import javarogue.generation.algorithms.GenerationAlgorithm;
import javarogue.generation.algorithms.Generation;
import javarogue.tileengine.Tile;
import javarogue.utility.Matrix;

public class Level {

	private Matrix<Tile> tileMap;
	private Matrix<Tile> objectMap;
	private GenerationAlgorithm generator;
	private int depth;

	/**
	 * 
	 * Builds a level of provided depth.
	 * 
	 * @param generator
	 * @param tileSet
	 */
	public Level(int depth) {
		this.depth = depth;
		this.generator = this.determineGeneration(this.depth);
	}

	public void generate() {
		this.tileMap = this.generator.generateTiles();
		this.objectMap = this.generator.generateObjects();
	}

	public Matrix<Tile> getTileMap() {
		return this.tileMap;
	}

	public Matrix<Tile> getObjectMap() {
		return this.objectMap;
	}

	private GenerationAlgorithm determineGeneration(int depth) {
		// TODO tmp impl
		int seed = 566754;
		GenerationAlgorithm gen;
		switch (depth) {
		case 0:
			gen = new Generation(30, 30, 8, seed);
			break;
		case 1:
			gen = new Generation(40, 40, 10, seed);
			break;
		case 2:
			gen = new Generation(50, 50, 12, seed);
			break;
		default:
			gen = null;
		}
		return gen;
	}

}
