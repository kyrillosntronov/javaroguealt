package javarogue.ui.config;

import java.util.Optional;

import javarogue.utility.Resolution;

public class ConfigModelImpl implements ConfigModel {

	private Optional<Resolution> resolution = Optional.empty();
	private Optional<String> tileSet = Optional.empty();
	
	@Override
	public void setResolution(Resolution resolution) {
		this.resolution = Optional.of(resolution);
	}

	@Override
	public Optional<Resolution> getResolution() {
		return this.resolution;
	}

	@Override
	public void setTileset(String path) {
		this.tileSet = Optional.of(path);
	}

	@Override
	public Optional<String> getTileset() {
		return this.tileSet;
	}

}
