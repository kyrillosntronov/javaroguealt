package javarogue.ui.game;

import java.util.Optional;

import javarogue.level.Level;

/**
 * 
 * Model of the Game Window, handles the level data.
 *
 */
public interface GameModel {

	/**
	 * 
	 * Generates the Level data
	 * 
	 * @param level Depth of the level to generate.
	 */
	public void generateLevel(int level);

	/**
	 * 
	 * @return Optional of the generated Level or Empty if none has been generated.
	 */
	public Optional<Level> getLevel();

}
