package javarogue.ui.game;

import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javarogue.config.ConfigGraphics;
import javarogue.level.Level;
import javarogue.ui.game.components.Camera;
import javarogue.ui.game.components.GUI;
import javarogue.ui.game.components.MiniMap;
import javarogue.utility.Position;
import javarogue.utility.Direction;

public class GameViewImpl implements GameView {

	private GameController controller;
	private GraphicsContext context;
	private Stage stage;
	
	private Camera camera;
	private MiniMap minimap;
	private GUI gui;
	
	@Override
	public void setController(GameController controller) {
		this.controller = controller;
	}

	@Override
	public void render() {
		Level level;
		if (!this.controller.getCurrentLevel().isPresent()) {
			throw new IllegalStateException("Cannot render non-existing level!");
		} else {
			level = this.controller.getCurrentLevel().get();
		}
		this.context.clearRect(0, 0, this.stage.getWidth(), this.stage.getHeight());
		this.camera.setLevel(level);
		this.camera.draw();
		this.minimap.setLevel(level);
		this.minimap.draw();
		this.gui.draw();
	}

	@Override
	public void open() {
		// Make window
		this.stage = new Stage();
		this.stage.setScene(this.buildScene());
		this.stage.setFullScreen(true);
		this.stage.setFullScreenExitHint("");
		this.stage.show();
		// Init components
		this.camera = new Camera(this.context);
		this.camera.setOrigin(new Position(0,0));
		this.minimap = new MiniMap(this.context);
		this.gui = new GUI(this.context);
	}

	@Override
	public void close() {
		this.stage.close();
	}

	private Scene buildScene() {
		Canvas canvas = new Canvas(ConfigGraphics.resolutionWidth, ConfigGraphics.resolutionHeight);
		this.context = canvas.getGraphicsContext2D();
		GridPane pane = new GridPane();
		pane.add(canvas, 0, 0);
		Scene scene = new Scene(pane);
		// TODO tmp implementation
		scene.setOnKeyPressed(e -> {
			switch (e.getCode()) {
			case SPACE:
				controller.generateLevel(0);
				break;
			case UP:
				this.camera.move(Direction.UP);
				break;
			case DOWN:
				this.camera.move(Direction.DOWN);
				break;
			case LEFT:
				this.camera.move(Direction.LEFT);
				break;
			case RIGHT:
				this.camera.move(Direction.RIGHT);
				break;
			case I:
				this.gui.changeInventoryState();
				break;
			case ESCAPE:
				this.stage.close();
				break;
			default:
			}
			this.render();
		});
		return scene;
	}

}
