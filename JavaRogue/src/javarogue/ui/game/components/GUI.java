package javarogue.ui.game.components;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javarogue.config.ConfigGraphics;

public class GUI {

	private Image gui;
	private GraphicsContext context;
	
	// TODO implement map with stat enum.
	private Map<Integer, String> overlayText;
	private Map<Integer, String> inventoryText;
	private Font font;
	
	// State booleans
	private boolean isInventoryOpened;
	
	public GUI(GraphicsContext context) {
		this.loadImage();
		this.initText();
		this.context = context;
	}

	public void draw() {
		double width = this.context.getCanvas().getWidth();
		double height = this.context.getCanvas().getHeight();
		if (this.isInventoryOpened) {
			this.context.setFill(Color.BLACK);
			this.context.fillRect(0, 0, width, height);
			this.context.setFill(Color.WHITE);
			// TODO tmp implementation
			this.context.fillText(this.inventoryText.get(0), 100, 100);
		} else {
			// TODO finalize when proper GUI image has been selected.
			this.context.drawImage(this.gui, 0, 0, width, height);
			this.context.setFont(this.font);
			this.context.setFill(Color.WHITE);
			// TODO implement when proper map is added.
			this.context.fillText(this.overlayText.get(0), 0,
					ConfigGraphics.resolutionHeight - ConfigGraphics.resolutionHeight / 10);
		}
	}
	
	public void changeInventoryState() {
		if(this.isInventoryOpened) {
			this.isInventoryOpened = false;
		} else {
			this.isInventoryOpened = true;
		}
	}

	private void loadImage() {
		try {
			this.gui = new Image(new FileInputStream(new File("res/GUI.png")), ConfigGraphics.resolutionWidth,
					ConfigGraphics.resolutionHeight, true, true);
		} catch (FileNotFoundException e) {
			throw new IllegalStateException("GUI.png not found!");
		}
	}
	
	private void initText() {
		try {
			this.font = Font.loadFont(new FileInputStream(new File("font/font.ttf")), 32);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.overlayText = new HashMap<>();
		this.overlayText.put(0, "Test");
		this.inventoryText = new HashMap<>();
		this.inventoryText.put(0, "Hello!");
	}
	
}
