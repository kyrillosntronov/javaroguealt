package javarogue.ui.game;

import java.util.Optional;

import javarogue.level.Level;

public class GameModelImpl implements GameModel {

	private Optional<Level> level = Optional.empty();
	
	@Override
	public void generateLevel(int level) {
		this.level = Optional.of(new Level(level));
		this.level.get().generate();
	}

	@Override
	public Optional<Level> getLevel() {
		return this.level;
	}
	
}
