package javarogue.generation.algorithms;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import javarogue.generation.corridor.Corridor;
import javarogue.generation.corridor.CorridorImpl;
import javarogue.generation.rooms.Room;
import javarogue.generation.rooms.SimpleRoom;
import javarogue.tileengine.Tile;
import javarogue.utility.Matrix;
import javarogue.utility.Position;

/**
 * 
 * Simple generation algorithm, creates connected rooms and fill them with
 * objects according to rules:
 * 
 * 
 * 
 */
public class Generation implements GenerationAlgorithm {

	// -- Matrixes --
	private Matrix<Tile> dungeon;
	private Matrix<Tile> objects;
	// -- Data Structures --
	private List<Room> rooms;
	// -- Parameters --
	private int roomNumber;
	private int width;
	private int height;
	// -- Seeded random number generator --
	private Random random;
	/**
	 * 
	 * Generates a dungeon of provided width and height with provided number of rooms.
	 * 
	 * @param width			Dungeon width.
	 * @param height		Dungeon height.
	 * @param roomNumber	Number of rooms.
	 */
	public Generation(int width, int height, int roomNumber, long seed) {
		this.rooms = new LinkedList<>();
		this.width = width;
		this.height = height;
		this.roomNumber = roomNumber;
		this.random = new Random();
		this.random.setSeed(seed);
	}

	@Override
	public Matrix<Tile> generateTiles() {
		// Create the level.
		dungeon = new Matrix<Tile>(this.height, this.width);
		// Prepare for generation
		dungeon.fill(Tile.VOID);
		// Make some simple rooms
		this.generateRooms(this.roomNumber);
		// Connect all the rooms with corridors
		this.connectRooms();
		// Add missing walls
		this.makeWalls();
		// Finally, return.
		return dungeon;
	}
	
	@Override
	public Matrix<Tile> generateObjects(){
		// Tiles must be generated
		if(this.dungeon == null) {
			throw new IllegalStateException("Cannot generate objects in an empty level");
		}
		// Create the objects layer
		objects = new Matrix<Tile>(this.height, this.width);
		// Prepare for overlaying
		objects.fill(Tile.ALPHA);
		/*
		// Make doors
		this.makeDoors();
		// Place Traps
		this.makeTraps();
		// Place loot
		this.makeLoot();
		// Generate room objects
		this.makeObjects();
		// Replace some objects for random objects
		this.makeAltObjects();
		// Place stairs
		this.makeStairs();
		// Place monsters
		this.makeMonsters();
		*/
		return this.objects;
	}

	// -- Generate Rooms --
	
	private void generateRooms(int num) {
		// Fail safe for faulty or impossible generations.
		int failsafe = 0;
		// Random origin to generate a room.
		Position randomOrigin;
		// While generated rooms are less than target room number...
		while(rooms.size() < num) {
			// Select a random point within game boundaries.
			randomOrigin = new Position(random.nextInt(this.height), random.nextInt(this.width));
			// Init a room
			Room room = new SimpleRoom(randomOrigin, this.dungeon, this.random);
			// Try to make a room
			if(room.canBeGenerated()) {
				room.generate();
				this.addTiles(room.getTileCoordinates());
				this.rooms.add(room);
			}
			// Catch impossible generations.
			failsafe++;
			if(failsafe > 1000) {
				// Hard reset.
				this.dungeon.fill(Tile.VOID);
				this.rooms.clear();
				failsafe = 0;
			}
		}
	}
	
	private Room selectRoom() {
		return null;
	}
	
	private void addTiles(Map<Position, Tile> tiles) {
		// Simply iterate the map and copy tiles on the matrix accordingly
		for(Entry<Position, Tile> entry : tiles.entrySet()) {
			this.dungeon.set(entry.getKey().getX(), entry.getKey().getY(), entry.getValue());
		}
	}

	// -- Connect Rooms --
	
	private void connectRooms() {
		// Make a corridor between every room pair
		for(int i = 0; i < this.rooms.size(); i++) {
			Position start = this.getRandomPoint(this.rooms.get(i));
			Position finish = this.getRandomPoint(this.rooms.get((i + 1) % this.rooms.size()));
			Corridor corridor = new CorridorImpl(start, finish, this.dungeon);
			corridor.generate();
		}
	}
	
	private Position getRandomPoint(Room room) {
		// Simply select a random floor tile
		return room.getFloor().get(random.nextInt(room.getFloor().size()));
	}
	
	// -- Clean up --

	private void makeWalls() {
		// Iterate over matrix
		this.dungeon.doubleFor((i, j) -> {
			// If a tile is FLOOR or WATER
			if (this.dungeon.get(i, j).equals(Tile.FLOOR) || this.dungeon.get(i, j).equals(Tile.WATER)) {
				// Check its neighbors
				for (Position pos : this.getNeighbors(new Position(i, j))) {
					// If a neighbor is VOID, set it to BLOCK
					if (this.dungeon.get(pos.getX(), pos.getY()).equals(Tile.VOID)) {
						this.dungeon.set(pos.getX(), pos.getY(), Tile.BLOCK);
					}
				}
			}
		});
	}

	private List<Position> getNeighbors(Position pos) {
		List<Position> neighbors = new LinkedList<>();
		//Check Moor neighbors:
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				// Handle bounds
				if (pos.getX() - 1 + i >= 0 && pos.getY() - 1 + j >= 0 && pos.getX() - 1 + i < this.dungeon.getRows()
						&& pos.getY() - 1 + j < this.dungeon.getCols()) {
					neighbors.add(new Position(pos.getX() - 1 + i, pos.getY() - 1 + j));
				}
			}
		}
		return neighbors;
	}

}