package javarogue.generation.corridor;

/**
 * 
 * A corridor is a game generation object that represents a path connecting two
 * rooms.
 *
 */
public interface Corridor {

	/**
	 * Generates the corridor on the referenced tile map.
	 * 
	 */
	public void generate();

}
